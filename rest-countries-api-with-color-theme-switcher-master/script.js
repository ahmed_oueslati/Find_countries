const getDataCountry = () => {
  fetch(`https://restcountries.com/v2/all`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data);

      const list = document.getElementById("countries");
      data.map((item) => {
        list.innerHTML += `  <article class="country">
        <div class="image">
      <img class="country__img" src="${item.flag}" />
      </div>
      <div class="country__data">${item.name}</h4>
      <p class="country__row">Population: ${item.population}</p>
      <p class="country__row">Region: ${item.region}</p>
      <p class="country__row">Capital: ${item.capital}</p>
      </div>
      </article>`;
      });
    });
};
getDataCountry();
//
const countryTofind = document.querySelector("#toSearch");

const searchForountry = function () {
  fetch(`https://restcountries.com/v2/all`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data);

      const list = document.getElementById("countries");
      list.innerHTML = "";
      data.filter((item) =>
        item.name.toUpperCase().startsWith(countryTofind.value.toUpperCase())
          ? (list.innerHTML += `  <article class="country">
          <div class="image">
          <img class="country__img" src="${item.flag}" />
          </div>
          <div class="country__data">${item.name}</h4>
          <p class="country__row">Population: ${item.population}</p>
          <p class="country__row">Region: ${item.region}</p>
          <p class="country__row">Capital: ${item.capital}</p>
          </div>
          </article>`)
          : console.log("none")
      );
    });
};

countryTofind.addEventListener("input", searchForountry);

/* const regionToFind = function () {
  fetch(`https://restcountries.com/v2/all`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const list = document.querySelector(".countries");
      list.innerHTML = "";
      data.filter((item) =>
        item.region
          ? (list.innerHTML += `  <article class="country">
    <img class="country__img" src="${item.flag}" />
    <div class="country__data">${item.name}</h4>
    <p class="country__row">Population: ${item.population}</p>
    <p class="country__row">Region: ${item.region}</p>
    <p class="country__row">Capita: ${item.capital}</p>
    </div>
    </article>`)
          : console.log("none")
      );
    });
}; */

const ToFindAfrica = document.getElementById("africa");
const ToFindAmerica = document.getElementById("america");
const ToFindAsia = document.getElementById("asia");
const ToFindEurope = document.getElementById("europe");
const ToFindOceania = document.getElementById("oceania");

ToFindAfrica.addEventListener("click", function () {
  fetch(`https://restcountries.com/v2/all`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const list = document.querySelector(".countries");
      list.innerHTML = "";
      data.filter((item) =>
        item.region === "Africa"
          ? (list.innerHTML += `  <article class="country">
          <div class="image">
    <img class="country__img" src="${item.flag}" />
    </div>
    <div class="country__data">${item.name}</h4>
    <p class="country__row">Population: ${item.population}</p>
    <p class="country__row">Region: ${item.region}</p>
    <p class="country__row">Capital: ${item.capital}</p>
    </div>
    </article>`)
          : console.log("none")
      );
    });
});
ToFindAmerica.addEventListener("click", function () {
  fetch(`https://restcountries.com/v2/all`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const list = document.querySelector(".countries");
      list.innerHTML = "";
      data.filter((item) =>
        item.region === "Americas"
          ? (list.innerHTML += `  <article class="country">
          <div class="image">
    <img class="country__img" src="${item.flag}" />
    </div>
    <div class="country__data">${item.name}</h4>
    <p class="country__row">Population: ${item.population}</p>
    <p class="country__row">Region: ${item.region}</p>
    <p class="country__row">Capital: ${item.capital}</p>
    </div>
    </article>`)
          : console.log("none")
      );
    });
});
ToFindAsia.addEventListener("click", function () {
  fetch(`https://restcountries.com/v2/all`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const list = document.querySelector(".countries");
      list.innerHTML = "";
      data.filter((item) =>
        item.region === "Asia"
          ? (list.innerHTML += `  <article class="country">
          <div class="image">
    <img class="country__img" src="${item.flag}" />
    </div>
    <div class="country__data">${item.name}</h4>
    <p class="country__row">Population: ${item.population}</p>
    <p class="country__row">Region: ${item.region}</p>
    <p class="country__row">Capital: ${item.capital}</p>
    </div>
    </article>`)
          : console.log("none")
      );
    });
});
ToFindEurope.addEventListener("click", function () {
  fetch(`https://restcountries.com/v2/all`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const list = document.querySelector(".countries");
      list.innerHTML = "";
      data.filter((item) =>
        item.region === "Europe"
          ? (list.innerHTML += `  <article class="country">
          <div class="image">
    <img class="country__img" src="${item.flag}" />
    </div>
    <div class="country__data">${item.name}</h4>
    <p class="country__row">Population: ${item.population}</p>
    <p class="country__row">Region: ${item.region}</p>
    <p class="country__row">Capital: ${item.capital}</p>
    </div>
    </article>`)
          : console.log("none")
      );
    });
});
ToFindOceania.addEventListener("click", function () {
  fetch(`https://restcountries.com/v2/all`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const list = document.querySelector(".countries");
      list.innerHTML = "";
      data.filter((item) =>
        item.region === "Oceania"
          ? (list.innerHTML += `  <article class="country">
          <div class="image">
    <img class="country__img" src="${item.flag}" />
    </div>
    <div class="country__data">${item.name}</h4>
    <p class="country__row">Population: ${item.population}</p>
    <p class="country__row">Region: ${item.region}</p>
    <p class="country__row">Capital: ${item.capital}</p>
    </div>
    </article>`)
          : console.log("none")
      );
    });
});

function myFunction() {
  var element = document.body;
  element.classList.toggle("dark");
}
